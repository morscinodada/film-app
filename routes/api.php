<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register','UserController@register');
Route::post('/login','UserController@login');

Route::post('/film/create','FilmController@createFilm');
Route::get('/films','FilmController@getAllFilms');
Route::get('/film/{slug}','FilmController@getSingleFilmBySlug');
Route::put('/film/edit/{slug}','FilmController@updateFilm');
Route::delete('/film/delete/{slug}','FilmController@deleteFilm');

Route::post('/film/add-comment/{id}','CommentController@addComment');
Route::get('/film/get-comments/{id}','CommentController@getComment');
Route::put('/film/update-comment/{film_id}/{comment_id}','CommentController@updateComment');
Route::delete('/film/delete-comment/{film_id}/{comment_id}','CommentController@deleteComment');

Route::get('/genres','GenreController@getAllGenre');
//Route::get('/film/{id}','FilmController@getSingleFilmById');