require('./bootstrap');


window.Vue = require('vue');

import vuetify from './vuetify';
import vuerouter from 'vue-router';
import vuex from 'vuex';
import App from './app.vue';
import routes from './routes.js';
import storedData from './store';
import vueCountry from 'vue-country-region-select';

Vue.use(vueCountry);
Vue.use(vuerouter);
Vue.use(vuex);
Vue.use(require('vue-truncate-filter'));
Vue.use(require('vue-moment'));

const store = new vuex.Store(storedData);
const router = new vuerouter({
  routes,
  mode:'history'
});

window.Vuetify = require('vuetify');
Vue.use(Vuetify);

export default Vue.extend({
    data(){
      return{
        country:'',
        region:''
      }
    }
})

new Vue({
  store,
  router,
  vuetify,
  render: h=> h (App)
}).$mount('#app');


