import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import Films from './components/Films/Main.vue';
import List from './components/Films/List.vue';
import New from './components/Films/New.vue';
import Film from './components/Films/View.vue';

const routes = [
  {path: '/', component: Home,name:'Home'},
  {path:'/register',component:Register,name:'Register'},
  {path:'/login',component:Login,name:'Login'},
  {
    path:'/films',
    component:Films,
   
    children:[
      {path: '/', component: List,name:'List'},
      {path: '/create', component: New,name:'NewFilm'},
      {path: ':slug', component: Film,name:'view'},
    
    ]
  },
];

export default routes