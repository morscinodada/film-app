import {getLocalUser} from './helpers/init.js';

const current_user = getLocalUser();

export default{
  state:{
    user:current_user,
    films:[],
    genres:[],
    snackBarStatus:false,
    snackbarType:'',
    snackbarMessage:''
  },
  getters:{
    getUser(state){
      return state.user;
    },
    films(state){
      return state.films;
    },
    genres(state){
      return state.genres;
    },
    snackBarStatus(state){
      return state.snackBarStatus;
    },
    snackbarType(state){
      return state.snackbarType;
    },
    snackbarMessage(state){
      return state.snackbarMessage;
    }
  },
  mutations:{
    logUserIn(state,data){
      state.user = Object.assign({},data.user,{token:data.token});
      localStorage.setItem('userToken',data.token);
      localStorage.setItem('user',JSON.stringify(state.user));
    },
    logUserOut(state){
      state.user = null;
      localStorage.removeItem('userToken');
      localStorage.removeItem('user');
    },
    getAllFilms(state){
      axios.get('api/films').then((response)=>{
        state.films = response.data.films;
        
      }).catch((error)=>{
        console.log(error);
      })
    },
    getAllGenres(state){
      axios.get('/api/genres').then((response)=>{
        state.genres = response.data.genres;
        console.log(response.data.genres);
      }).catch((error)=>{
        console.log(error);
      })
    },
    showMessage(state,payload){
      state.snackbarMessage= payload.message;
      state.snackbarType= payload.type;
      state.snackBarStatus= payload.status;
    },
    killSnanckbar(state){
      state.snackBarStatus = false;
    },
    sortFilms(state,data){
       state.films.sort((a,b) => a[data] < b[data] ? -1: 1);
    }
  },
  actions:{
    getAllFilms(context){
      context.commit('getAllFilms');
    },
    getAllGenres(context){
      context.commit('getAllGenres');
    }
  }
}
