
## The Film App by Dada Babatunde

The film app is an application that allows a user view uploaded films, create a film and allows authenticated users make 

comments on a film, It also ha a powerful user authentucatin system powered by the JWT authentication.

It is built with PHP/Laravel framework for its backend and Vue Js handled its frontend, it also uses the Vuetify and material 

design components for its user interface. 

## Installation Requirements

- Your PC must be connected to the internet

- PHP 7.1 and above must be intstalled on your PC

  You can click on this link to download and install PHP https://www.php.net/downloads.php

- MySql 5.6 and above must be running on your PC

  You can click on this link to download and install MySql https://www.mysql.com/downloads/

- Composer version 1.9.0 and above must be installed on your PC

  You can follow this link to download and install composer https://getcomposer.org/

- Git must be installed on your PC 

  You can follow this link to download install Git https://git-scm.com/downloads 

- Node Js Version 6.1.0 and above must be installed on your PC

  You can follow this link to download install Node Js https://nodejs.org/en/download/

## Installation and Setup

The various steps involved to setup and run this application are listed below.
- Clone Git Repository
  
  Open the command terminal on your PC and cd to your preferred directory, the run the git command bellow to clone/download the

  application's folders and files

  git clone https://morscinodada@bitbucket.org/morscinodada/film-app.git 

  cd to the applicaton's root directory, the root directory has folders like app,bootstrap,config,database,etc.

``cd film-app``

- Connect to database

  Open up your mysql database manager (I use Mysql workbench) and create the application's database,

  go the root directory, duplicate the .env.example file,and name yours .env , 

  open the duplicated .env file with your favorite text editor,

  change the value of the DB_HOST,DB_DATABASE, DB_USERNAME and DB_PASSWORD to what you have set up, and save.

  ``DB_CONNECTION=mysql``

 `` DB_HOST=127.0.0.1``

  ``DB_PORT=3306``

  ``DB_DATABASE=your_database_name``

 `` DB_USERNAME=your_database_username``

  ``DB_PASSWORD=your_database_password`` 
  

- Add Authentication Token and application's key

  Still in the newly created .env file copy, paste and save the follwing code into the .env file

  ``` JWT_SECRET=FYSru6ZQ131u3wu2eCJ6lop4sgUCRueI ```  

  also replace

  ``APP_KEY=`` 

  with

  ```APP_KEY=base64:omG+n9724O7hewphlGnFV+d/FfVMZywRZcDitisVXro=```



- Run Migrations

  While still in the application's root directory, 

  run the following command from your cmd to create all necessary tables and 

  columns. 

  ```  php artisan migrate ```

- Run DB seed  

  While still in the application's root directory, run the following command from your cmd to create dummy data needed for 

  test in the application.

   ```  php artisan db:seed ```

- Install Vue Js and all its dependencies
  
   From your command line Run the command below to install all javascript packages needed by the application  

   ```  npm install --save ```

- Serve Application

  From your command line 

  Run ``php artisan serve``  to host the application.

  It would most likely be hosted on http://localhost:8000 on your PC,

   copy whatever url it provides as its hosting url into your browser and that's it...


   Eureka!!!

