<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
  protected $fillable = [
        'name','reply_to', 'comment'
    ];
}
