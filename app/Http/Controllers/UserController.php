<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
   public function register(Request $request)
    {
        
        //Validate User Input
        $validator = Validator::make($request->all(),[
            'firstname'=>'required|string|max:255',
            'lastname'=>'required|string|max:255',
            'username'=>'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
             ]
        );

        if( $validator->fails()){
            return response()->json(['error'=> $validator->errors()],500);
        }
        $user = User::create([
            'lastname'=>$request->lastname,
            'firstname'=>$request->firstname,
            'username'=>$request->username,
            'password'=>bcrypt($request->password),
            'email'=>$request->email
            ]
        );

        $token = JWTAuth::fromUser($user);
        if(!$token){
            return response()->json(['error'=>'an error occured while creating user'],500);
        }

        return response()->json(compact('user','token'),201);

    }


     public function login(Request $request)
    {
        $credentials = $request->all();
         //Validate User Input
        $validator = Validator::make($credentials,[
            'username'=>'required|string|max:255|min:6',
            'password' => 'required|string|min:6',
             ]
        );

        if( $validator->fails()){
            return response()->json(['error'=> $validator->errors()],500);
        }

        

        if(!$token = auth()->attempt($credentials)){
            return response()->json(['error'=>['login_error'=>'Invalid username or password']],500);
        }
        $user =  auth()->user();

        return response()->json(compact('user','token'),200);
    }
}
