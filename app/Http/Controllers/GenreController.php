<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    //
  public function getAllGenre(){
    $genres = Genre::all();

    return response()->json(compact('genres'));
  }
}
