<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FilmController;
use Illuminate\Support\Facades\Validator;
use App\Film;

class CommentController extends Controller
{
    //
    public function addComment(Request $request,$slug){
        $user = $this->authUser();
        // print_r($user->username);
        // exit();
        if(!$user){
          return response()->json(['error'=> 'Unauthorized User'],500);
        }

        $validator = Validator::make($request->all(),[
            'comment'=>'required|string|max:255',
             ]
        );

        if( $validator->fails()){
            return response()->json(['error'=> $validator->errors()],500);
        }

        $film = Film::where('slug',$slug)->firstOrFail();

        $credentials = $request->all();
        $credentials['name'] = $user->username;

        if(!$newComment = $film->comments()->create($credentials)){
            return response()->json(['error'=>'Could not create New Comment'],500);
         } 
           //return $user;
        return response()->json(['comment'=>$newComment],201);
    }

    public function getComment($id){
        // $user = $this->authUser();

        // if(!$user){
        //   return response()->json(['error'=> 'Unauthorized User'],500);
        // }

        $film = Film::where('id',$id)->firstOrFail();

        if (!$comments = $film->comments()->orderBy('id','DESC')->get()){
             return response()->json(['error'=>'Could not get comments'],500);
        }

        return response()->json(compact('comments'),200);
    }

    public function updateComment(Request $request,$film_id,$comment_id){
        $user = $this->authUser();

        if(!$user){
            return response()->json(['error'=> 'Unauthorized User'],500);
        }
        $film = Film::where('id',$film_id)->firstOrFail();

        $newDetails = $request->only(['comment']);

        if(!$updatedComment = $film->comments()->whereId($comment_id)->update($newDetails)){
            return response()->json(['error'=>'Could not Update Comment'],500);
        } 

      

        return  response()->json(['comment'=>$updatedComment],200);
    }

     public function deleteComment(Request $request,$film_id,$comment_id){
        $user = $this->authUser();

        if(!$user){
          return response()->json(['error'=> 'Unauthorized User'],500);
        }
        $film = Film::where('id',$film_id)->firstOrFail();

        if(!$deletedComment = $film->comments()->whereId($comment_id)->delete()){
            return response()->json(['error'=>'Could not Delete Comment'],500);
        }

        return  response()->json(['comment'=>$deletedComment],200);
     }
}
