<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\CommentController;
use App\Film;

class FilmController extends Controller
{
    //
  public function createFilm(Request $request){
   
    $validator = Validator::make($request->all(),[
        'name'=>'required|string|max:255',
        'description'=>'required|string',
        'rating'=>'required|integer',
        'ticket_price' => 'required|integer',
        'release_date' => 'required|string'
         ]
    );

    if( $validator->fails()){
        return response()->json(['error'=> $validator->errors()],500);
    }

    $result = date_format(date_create($request->release_date),'Y-m-d h:i:s'); 
   
       
    $slug = $this->createSlug($request->name);

    $image = $request->file('image');

    $extension = $image->getClientOriginalExtension();

    $image_name = 'profile'.time().'.'.$extension;
    // return $image_name;
    // exit();
    Storage::disk('public')->put($image_name,File::get($image));
    //Create film
     $newfilm = Film::create([
        'name'=>$request->name,
        'description'=>$request->description,
        'rating'=>$request->rating,
        'ticket_price'=>intval($request->ticket_price),
        'release_date'=>$result,
        'country'=>$request->country,
        'slug'=>$slug,
        'photo'=>'/uploads/'.$image_name
        ]);

     if(!$newfilm ){
            return response()->json(['error'=>'Could not create New Film'],500);
         } 
         //Add genres
          $genres = explode(',',$request->genres);
         foreach ($genres as $key => $genre) {
           $genres[$key] = intval($genre);
         }
         
          $film = Film::where('slug',$slug)->firstOrFail();
          $film->genres()->sync( $genres);

         //return new film;
        return response()->json(['film'=>$newfilm],201);
     }

  public function getAllFilms(){
      $films = Film::all();

      foreach ($films as $film) {
       $film['genres'] = $this->getfilmGenres($film);
      }

     if(!$films ){
          return response()->json(['error'=>'Could not get films'],500);
      } 
     return response()->json(compact('films'),201);
  }

  public function getSingleFilmBySlug($slug){
      if(!$slug){
        return response()->json(['error'=>'Something went wrong'],500);
      } 

      $comment = new CommentController();

      $film = Film::where('slug',$slug)->firstOrFail();
      $film['genres'] = $this->getfilmGenres($film);
      $film['comments'] = $comment->getComment($film['id']);


      if(!$film){
        return response()->json(['error'=>'Could not get film'],500);
      } 
      
      return response()->json(compact('film'),200);
  }

  private function getfilmGenres($film){
    return $film->genres;
  }

  public function updateFilm(Request $request,$slug){
     $user = $this->authUser();
     if(!$user){
      return response()->json(['error'=> 'Unauthorized User'],500);
     }
    $user = $this->authUser();

    if(!$user){
      return response()->json(['error'=> 'Unauthorized User'],500);
    }
    $film = Film::where('slug',$slug)->firstOrFail();
    //$film = $this->getSingleFilmBySlug($slug);

    // if($film->name != $request->name){
    //   $film->slug = $this->createSlug($request->name);
    // }
    // print_r($request->genres);
    // exit();

    $film->genres()->sync($request->genres);
    $film->name = $request->name;
    $film->description = $request->description;
    $film->rating = $request->rating;
    $film->ticket_price = $request->ticket_price;
    $film->release_date = date_format(date_create($request->release_date),'Y-m-d h:i:s');
    $film->country = $request->country;

    $updatedFilm = $film->save();

    //update genres


    if(!$updatedFilm){
        return response()->json(['error'=>'Could not update film'],500);
    } 
      
    return response()->json(['film'=>$updatedFilm],201);
  }

  public function deleteFilm($slug){
     $user = $this->authUser();
     if(!$user){
      return response()->json(['error'=> 'Unauthorized User'],500);
     }
      $user = $this->authUser();

      if(!$user){
        return response()->json(['error'=> 'Unauthorized User'],500);
      }
      $film = Film::where('slug',$slug)->firstOrFail();

      //remove film photo
      File::delete($film->photo);

      if(!$deletedFilm = $film->delete()){
         return response()->json(['error'=>'Could not Delete Film'],500);
       } 

      return response()->json(['film'=> $deletedFilm],200);
  }

  // public function getSingleFilmById($id){
  //   if(!$id){
  //       return response()->json(['error'=>'Something went wrong'],500);
  //     } 

  //     $film = Film::where('id',$id)->firstOrFail();
  //     $film['genres'] = $this->getfilmGenres($film);

  //     if(!$film){
  //       return response()->json(['error'=>'Could not get film'],500);
  //     } 
  //      return response()->json(compact('film'),201);
  // }

  public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }
        // Just append numbers until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Film::select('slug')->where('slug', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }

}
