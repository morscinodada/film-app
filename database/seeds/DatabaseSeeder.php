<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //clear table at every new seeding
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
      DB::table('genres')->truncate();
      DB::table('films')->truncate();
      DB::table('comments')->truncate();
        // $this->call(UsersTableSeeder::class);

      //seed genre data
      factory(App\Genre::class,6)->create();

      //seed film and comments data
      factory(App\Film::class,3)->create()->each(function($film){
        $film->comments()->saveMany(factory(App\Comment::class,6)->make());
      });

      //seed film_genre pivot table
      $genres = App\Genre::all();
      App\Film::all()->each(function($film) use ($genres){
        $film->genres()->saveMany($genres);
      });

    }
}
