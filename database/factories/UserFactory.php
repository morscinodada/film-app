<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(App\User::class, function (Faker $faker) {
//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\Film::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'release_date' => $faker->date, // secret
        'rating' => $faker->numberBetween(1,5),
        'ticket_price' => $faker->numberBetween(500,2000),
        'country' => $faker->country,
        'photo' => '/uploads/photo.jpg',
        'slug'=>$faker->slug
    ];
});

$factory->define(App\Genre::class, function (Faker $faker) {
     
    return [
        'name' => $faker->word
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName,
        'comment' => $faker->paragraph,
       
    ];
});
